package com.entfrm.biz.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.cms.entity.Category;

/**
 * @author entfrm
 * @date 2020-03-31 22:57:28
 *
 * @description 类别Mapper接口
 */
public interface CategoryMapper extends BaseMapper<Category>{

}
