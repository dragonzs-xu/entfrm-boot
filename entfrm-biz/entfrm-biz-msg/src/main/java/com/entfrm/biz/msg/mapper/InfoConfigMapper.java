package com.entfrm.biz.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.msg.entity.InfoConfig;

/**
 * @author entfrm
 * @date 2020-05-10 14:53:43
 * @description 消息配置Mapper接口
 */
public interface InfoConfigMapper extends BaseMapper<InfoConfig> {

}
