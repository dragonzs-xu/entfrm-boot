package com.entfrm.biz.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.msg.entity.InfoPush;

/**
 * @author entfrm
 * @date 2020-05-24 22:26:59
 *
 * @description 消息推送Mapper接口
 */
public interface InfoPushMapper extends BaseMapper<InfoPush>{

}
