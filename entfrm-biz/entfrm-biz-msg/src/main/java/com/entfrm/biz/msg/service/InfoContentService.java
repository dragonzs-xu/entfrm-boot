package com.entfrm.biz.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.msg.entity.InfoContent;

/**
 * @author entfrm
 * @date 2020-05-23 12:14:03
 * @description 消息内容Service接口
 */
public interface InfoContentService extends IService<InfoContent> {
}
